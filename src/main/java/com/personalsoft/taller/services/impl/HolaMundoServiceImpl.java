package com.personalsoft.taller.services.impl;

import org.springframework.stereotype.Service;

import com.personalsoft.taller.services.HolaMundoService;

@Service
public class HolaMundoServiceImpl implements HolaMundoService {

	@Override
	public String getSaludo() {
		return "Hola Mundo";
	}

}
