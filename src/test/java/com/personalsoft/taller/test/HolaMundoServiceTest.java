package com.personalsoft.taller.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.personalsoft.taller.controller.HolaMundoController;
import com.personalsoft.taller.services.HolaMundoService;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class HolaMundoServiceTest {

	@Autowired
	HolaMundoService holaMundoService;
	@Autowired
	private HolaMundoController controladorHolaMundo;
	@Mock
	HolaMundoService holaMundoServiceMock;
	
	
	@BeforeEach
	public void estemetodoseiniciaprimero() {
	}

	@Test
	public void itShouldSayHolaMundo() {
		assertEquals("Hola Mundo", holaMundoService.getSaludo());
	}

	@Test
	public void itShouldReturnTheServiceValueWith200StatusCode() {
		Mockito.when(holaMundoServiceMock.getSaludo()).thenReturn("Hola Mundo");
		ResponseEntity<String> httpResponse = controladorHolaMundo.sayHello();

		assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
		assertEquals("Hola Mundo", httpResponse.getBody());
	}
}
