package com.personalsoft.taller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonalsoftTallerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonalsoftTallerApplication.class, args);
	}

}
