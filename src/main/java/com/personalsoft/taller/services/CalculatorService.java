package com.personalsoft.taller.services;

public interface CalculatorService{
		
	public int suma(int a, int b);
	public int resta(int a, int b);
	public int dividir (int a, int b);
    public int multiplicar(int a, int b);	

}
