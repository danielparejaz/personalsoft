package com.personalsoft.taller.services.impl;

import org.springframework.stereotype.Service;

import com.personalsoft.taller.services.CalculatorService;

@Service
public class CalculatorServiceImpl implements CalculatorService{

	 
	
	@Override
	public int suma(int a, int b) {
		int resultado=a+b;
		return resultado;
	}

	@Override
	public int resta(int a, int b) {
		return a-b;
	}

	@Override
	public int dividir(int a, int b) {
		return a/b;
	}

	@Override
	public int multiplicar(int a, int b) {
		return a*b;
	}

}
