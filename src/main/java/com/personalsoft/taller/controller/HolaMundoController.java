package com.personalsoft.taller.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personalsoft.taller.services.HolaMundoService;

@RestController
public class HolaMundoController {

	@Autowired
	HolaMundoService holaMundoService;

	@GetMapping("/saludo")
	public ResponseEntity sayHello() {
		return ResponseEntity.ok(holaMundoService.getSaludo());
	}

}
