package com.personalsoft.taller.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.personalsoft.taller.services.CalculatorService;


@SpringBootTest
@ExtendWith(SpringExtension.class)
public class CalculatorServicesTest {

	
	@Autowired
	CalculatorService calculatorService;
	
	@BeforeEach
	public void metodoqueiniciaprimero() {
		System.out.println("Antes de cada prueba");
	}
	
	
	@Test
	public void pruebadelasuma() {
		System.out.println("Prueba suma");
		//dado que:
		int a=2;
		int b=2;
		
		//Cuando
		int resultado=calculatorService.suma(a, b);
		
		//Entonces...Testeamos
		assertEquals(4, resultado);
		
	}
	
	@AfterEach
	public void metodoqueiniciaalfinal()
	{
		System.out.println("Despu�s de cada prueba");
	}
	
	
	
}
